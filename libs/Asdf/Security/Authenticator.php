<?php
namespace Asdf\Security;

use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\Selection;
use Nette\Object;
use Nette\Security as NS;

/**
 * Users authenticator.
 *
 * @author     John Doe
 * @package    MyApplication
 */
class Authenticator extends Object implements IAuthenticator
{
	/** @var Selection */
	private $tableUsers;



	public function __construct (Selection $tableUsers)
	{
		$this->tableUsers = $tableUsers;
	}

	/**
	 * Performs an authentication
	 * @param  array
	 * @return \Nette\Security\Identity
	 * @throws \Nette\Security\AuthenticationException
	 */
	public function authenticate (array $credentials)
	{
		list($username, $password) = $credentials;
		$row = $this->tableUsers->where('login = ? AND password IS NOT NULL', $username)->fetch();

		if (!$row) {
			throw new NS\AuthenticationException("User '$username' not found.", self::IDENTITY_NOT_FOUND);
		}

		if ($row->password !== $this->calculateHash($password)) {
			throw new NS\AuthenticationException("Invalid password.", self::INVALID_CREDENTIAL);
		}

		return $this->mapRowToIdentity($row);
	}

	/**
	 * Performs an authentication
	 * @param  string $email
	 * @return \Nette\Security\Identity
	 * @throws \Nette\Security\AuthenticationException
	 */
	public function authenticateViaEmail ($email)
	{
		$row = $this->tableUsers->where('email = ? AND email IS NOT NULL', $email)->fetch();

		if (!$row) {
			throw new NS\AuthenticationException("User with email '$email' not found.", self::IDENTITY_NOT_FOUND);
		}

		return $this->mapRowToIdentity($row);
	}

	/**
	 * @param ActiveRow $row
	 * @return \Nette\Security\Identity
	 */
	private function mapRowToIdentity (ActiveRow $row)
	{
		unset($row->password);
		return new NS\Identity($row->id, $row->role, $row->toArray());
	}


	/**
	 * Computes salted password hash.
	 * @param  string
	 * @return string
	 */
	public function calculateHash ($password)
	{
		return md5($password . str_repeat('asdfbWER34TZGB2čščzrthg654351řhtfd', 10));
	}

}
