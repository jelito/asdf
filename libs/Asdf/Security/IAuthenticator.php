<?php
namespace Asdf\Security;

interface IAuthenticator extends \Nette\Security\IAuthenticator
{

	/**
	 * Performs an authentication
	 * @param  string $email
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticateViaEmail ($email);
}
