<?php
namespace Asdf\Security;

class User extends \Nette\Security\User
{
	private $login;
	
	public function loginViaEmail ($email)
	{
		$this->logout(TRUE);
		$id = $this->getAuthenticator()->authenticateViaEmail($email);
		$this->storage->setIdentity($id);
		$this->storage->setAuthenticated(TRUE);
		$this->onLoggedIn($this);
	}
	
	public function getLogin ()
	{
		if ($this->login === NULL) {
			$this->login = FALSE;
			$identity = $this->getIdentity();
			if ($identity) {
				$data = $identity->getData();
				if (isset($data['login'])) {
					$this->login = $data['login'];
				}
			}
		}
		
		return $this->login;
	}
	
}