<?php
namespace Asdf\Security;

interface IAuthorizator extends \Nette\Security\IAuthorizator
{

	/**
	 * nsatavi role, zdroje a opravneni
	 *
	 * @param array $roles	   pole roli
	 * @param array $resources pole zdroju
	 * @param array $access	   opravneni
	 *
	 * @return void
	 */
	public function __construct (array $roles, array $resources, array $access);

	/**
	 * Returns TRUE if the Resource exists in the list.
	 * @param  string $resource
	 * @return bool
	 */
	public function hasResource($resource);

}