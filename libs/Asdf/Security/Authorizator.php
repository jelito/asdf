<?php
namespace Asdf\Security;

use Nette\Security\Permission;

class Authorizator extends Permission implements IAuthorizator
{

	/**
	 * nsatavi role, zdroje a opravneni
	 *
	 * @param array $roles	   pole roli
	 * @param array $resources pole zdroju
	 * @param array $access	   opravneni
	 *
	 * @return void
	 */
	public function __construct (array $roles, array $resources, array $access)
	{
		$this->setRoles($roles);
		$this->setResources($resources);
		$this->setAccess($access);
	} // end

	/**
	 * nastavi role
	 *
	 * @param $roles array
	 *       	 pole roli
	 *
	 * @return void
	 */
	protected function setRoles (array $roles)
	{
		foreach ($roles as $role) {
			$parent = isset($role['parent']) ? $role['parent'] : NULL;
			$this->addRole($role['name'], $parent);
		}
	}

	/**
	 * nastavi zdroje
	 *
	 * @param array $resources pole zdroju
	 *
	 * @return void
	 */
	protected function setResources (array $resources)
	{
		foreach ($resources as $resource) {
			$parent = isset($resource['parent']) ? $resource['parent'] : NULL;
			$this->addResource($resource['name'], $parent);
		}

	}

	/**
	 * nastavi opravneni
	 *
	 * @param array $access pole opravneni
	 *
	 * @return void
	 */
	protected function setAccess (array $access)
	{
		foreach ($access as $roleName => $rights) {
			if (isset($rights['allow'])) {
				foreach ($rights['allow'] as $right) {
					$resourceName = isset($right['resource']) ? $right['resource'] : NULL;
					$privilege = isset($right['privilege']) ? $right['privilege'] : NULL;
					$this->allow($roleName, $resourceName, $privilege);
				}
			}

			if (isset($rights['deny'])) {
				foreach ($rights['deny'] as $right) {
					$resourceName = isset($right['resource']) ? $right['resource'] : NULL;
					$privilege = isset($right['privilege']) ? $right['privilege'] : NULL;
					$this->deny($roleName, $resourceName, $privilege);
				}
			}
		}
	}
}