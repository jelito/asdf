<?php
namespace Asdf\Caching\Storages;

use Nette\Object;
use Nette\Caching\IStorage;
use Predis\Client;

class RedisStorage extends Object implements IStorage
{
	private $client;
	
	public function __construct(array $params, array $options)
	{
		$this->client = new Client(
			$params,
			$options
		);
	}
	
	public function getClient()
	{
		return $this->client;
	}
	
	public function read($key)
	{
		return $this->client->get($key);
	}

	public function lock($key)
	{
	}

	public function write($key, $data, array $dependencies)
	{
		if (is_array($dependencies) && count($dependencies)) {
			throw new \Exception("dependencies are not allowed");
		}
		return $this->client->set($key, $data);
	}

	public function remove($key)
	{
		return $this->client->del($key);
	}

	public function clean(array $conds)
	{
		return $this->client->flushAll();
	}
}
