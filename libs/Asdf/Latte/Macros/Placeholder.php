<?php
namespace Asdf\Latte\Macro;

use Latte\CompileException;
use Latte\IMacro;
use Latte\MacroNode;
use Latte\PhpWriter;
use Nette\Utils\Strings;

class Placeholder implements IMacro
{
	/**
	 *
	 */
	function initialize()
	{
	}

	/**
	 * @return array|void
	 */
	function finalize()
	{
	}

	/**
	 * @param MacroNode $node
	 * @throws CompileException
	 * @return bool|void
	 */
	function nodeOpened(MacroNode $node)
	{
		$pair = $node->tokenizer->fetchWord();
		if ($pair === FALSE) {
			throw new CompileException("Missing placeholder name in {placeholder}");
		}

		$phpWriter = PhpWriter::using($node);

		$pair = explode(':', $pair, 2);
		$name = $phpWriter->formatWord($pair[0]);
		$method = isset($pair[1]) ? ucfirst($pair[1]) : '';
		$method = Strings::match($method, '#^\w*\z#') ? "render$method" : "{\"render$method\"}";
		$param = $phpWriter->formatArray();
		if (!Strings::contains($node->args, '=>')) {
			$param = substr($param, 6, -1); // removes array()
		}
		$return = '';
		$return .= "<?php foreach(\$placeholder->getWidgets($name) as \$widget) {";
		$return .= "\$widget->$method($param);";
		$return .= "} ?>";

		$node->isEmpty = true;
		$node->openingCode = $phpWriter->write($return);
	}

	/**
	 * @param MacroNode $node
	 */
	function nodeClosed(MacroNode $node)
	{
	}
}