<?php
namespace Asdf\Application\UI;

use Nette\Application\UI\Presenter;

abstract class BasePresenter extends Presenter
{
	/**
	 * @inheritdoc
	 */
	protected function createTemplate ($class = NULL)
	{
		$template = parent::createTemplate();
		$this->getPluginManager()->onPresenterCreateTemplate($this, $template);
		return $template;
	}
	/**
	 * @inheritdoc
	 */
	protected function startup ()
	{
		parent::startup();
		$this->getPluginManager()->onPresenterStartup($this);
	}
	/**
	 * @inheritdoc
	 */
	protected function beforeRender ()
	{
		parent::beforeRender();
		$this->getPluginManager()->beforePresenterRenderAction($this);
	}
	/**
	 * @inheritdoc
	 */
	protected function afterRender ()
	{
		parent::afterRender();
		$this->getPluginManager()->afterPresenterRenderAction($this);
	}

	/**
	 * @return \Asdf\Application\Plugin\PluginManager
	 */
	protected function getPluginManager()
	{
		return $this->context->pluginManager;
	}

	/**
	 * @return \Asdf\Security\User
	 */
	public function getUser ()
	{
		return parent::getUser();
	}
}
