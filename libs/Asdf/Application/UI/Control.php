<?php
namespace Asdf\Application\UI;

abstract class Control extends \Nette\Application\UI\Control
{

	/**
	 * @param  string|NULL
	 * @return Nette\Templating\ITemplate
	 */
	protected function createTemplate ($class = NULL)
	{
		$template = parent::createTemplate('Asdf\Templating\FileTemplate');
		$this->getPluginManager()->onControlCreateTemplate($this, $template);
		return $template;
	}

	/**
	 * @return \Asdf\Application\PluginManager
	 */
	protected function getPluginManager()
	{
		return $this->getPresenter()->getContext()->pluginManager;
	}
}