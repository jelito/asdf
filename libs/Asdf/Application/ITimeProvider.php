<?php
namespace Asdf\Application;

interface ITimeProvider
{
	/**
	 * Returns formated datetime constant for whole code run
	 *
	 * @return \Nette\DateTime
	 */
	public function getConstantDateTime ();
	
	/**
	 * Returns date like timestamp constant for whole code run
	 *
	 * @return int
	 */
	public function getConstantTimestamp ();
}