<?php
namespace Asdf\Application\Widget;

use Nette\Object;

class Placeholder extends Object
{
	private $widgets = array();

	public function addWidget(Widget $widget)
	{
		$this->widgets[] = $widget;
		return $this;
	}

	public function getWidgets($place)
	{
		$widgets = array();
		foreach ($this->widgets as $widget) {
			foreach ($this->getPositions($widget) as $positionInfo) {
				if ($positionInfo->place == $place) {
					$this->addWidgetIntoArray($widgets, $widget, $positionInfo);
				}
			}
		}
		ksort($widgets);
		return $widgets;
	}

	private function getPositions(Widget $widget)
	{
		$positionInfoArray = $widget->getPositionInfo();
		if (!is_array($positionInfoArray)) {
			return array($positionInfoArray);
		}
		return $positionInfoArray;
	}

	private function addWidgetIntoArray(array &$widgets, Widget $widget, PositionInfo $positionInfo)
	{
		$position = $positionInfo->position;
		if ($position === null) {
			$widgets[] = $widget;
			return;
		}

		if (isset($widgets[$position])) {
			trigger_error(
				"Widget named " . get_class($widget) . " has same position '{$position}'",
				E_USER_WARNING
			);
			$widgets[] = $widget;
			return;
		}
		$widgets[$position] = $widget;
	}
}