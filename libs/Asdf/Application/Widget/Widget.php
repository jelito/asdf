<?php
namespace Asdf\Application\Widget;

use Asdf\Application\UI\Control;
use Nette\ComponentModel\IContainer;

abstract class Widget extends Control
{
	public function __construct()
	{
	}

	final public function setParent(IContainer $parent = NULL, $name = NULL)
	{
		parent::setParent($parent, $name);
		$explode = explode('\\', get_class($this));
		$name = strtolower(str_replace('Widget', '', end($explode)));
		$dir = dirname($this->getReflection()->getFileName());
		$this->template->setFile("$dir/$name.latte");
		$this->init();
	}

	abstract public function getPositionInfo();

	protected function init()
	{
	}

	protected function beforeRender()
	{
	}

	public function render()
	{
		$this->beforeRender();
		$this->template->render();
	}
}