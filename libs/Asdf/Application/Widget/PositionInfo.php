<?php
namespace Asdf\Application\Widget;

class PositionInfo
{
	public $place;
	public $position;

	public function __construct($place, $position = null)
	{
		$this->place = $place;
		$this->position = $position;
	}
}