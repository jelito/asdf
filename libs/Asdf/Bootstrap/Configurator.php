<?php
namespace Asdf;

define('WWW_DIR', realpath(BASE_DIR . '/www'));
define('APP_DIR', realpath(BASE_DIR . '/app'));
define('LIBS_DIR', realpath(BASE_DIR . '/libs'));
define('LOG_DIR', realpath(BASE_DIR . '/log'));
define('TEMP_DIR', realpath(BASE_DIR . '/temp'));

require_once LIBS_DIR . '/Nette/loader.php';

use Asdf\Diagnostics\DevNullBar;
use Nette\Application\UI\Presenter;
use Nette\DI\Config\Loader;
use Tracy\Debugger;
use Tracy\Helpers as TracyHelpers;
use Nette\Application\Application;
use Nette\Application\Request;
use Nette\Application\IResponse;

class Configurator extends \Nette\Configurator
{
	private $directories;
	private $sectionName;
	private $localNeonPath;
	private $localNeonExists;
	private $configNeonPath;
	private $bootstrapParams;

	const DEFAULT_SECTION = 'production';

	public function __construct ()
	{
		parent::__construct();

		$this->directories = array(
			'baseDir' => BASE_DIR,
			'logDir' => LOG_DIR,
			'libsDir' => LIBS_DIR,
			'appDir' => APP_DIR,
			'wwwDir' => WWW_DIR,
			'tempDir' => TEMP_DIR,
		);

		$this->initDefaultConfiguratorParams();
		$this->setBootstrapConfigParams();
		$this->initDebug();
		$this->initRobotLoader();
		$this->setConfigs();
	}

	private function initDefaultConfiguratorParams ()
	{
		$this->addParameters(
			$this->directories
		);
		$this->setTempDirectory($this->directories['tempDir']);

		$this->configNeonPath = APP_DIR . '/config/config.neon';
		$this->localNeonPath = APP_DIR . '/config/local.neon';
		$this->localNeonExists = file_exists($this->localNeonPath);
	}

	private function setBootstrapConfigParams()
	{
		$loader = new Loader();
		if ($this->localNeonExists) {
			$sectionConfig = $loader->load($this->localNeonPath, 'bootstrap-section');
			$this->sectionName = $sectionConfig['name'];
		} else {
			$this->sectionName = self::DEFAULT_SECTION;
		}

		$mainParams = $loader->load($this->configNeonPath, $this->sectionName);
		$this->bootstrapParams = $mainParams['parameters']['bootstrap'];

		if ($this->localNeonExists) {
			$localParams = $loader->load($this->localNeonPath, $this->sectionName);
			$localParams = isset($localParams['parameters']['bootstrap']) ? $localParams['parameters']['bootstrap'] : array();
			$this->bootstrapParams = \Nette\DI\Config\Helpers::merge($localParams, $this->bootstrapParams);
		}
	}

	private function initDebug ()
	{
		$debuggerParams = $this->bootstrapParams['debugger'];
		$this->setDebugMode($debuggerParams['enabled']);
		Debugger::$strictMode = $debuggerParams['strictMode'];
		Debugger::enable(
			$this->parameters['productionMode'],
			$this->insertDirectoryPath($debuggerParams['logDirectory']),
			$this->insertDirectoryPath($debuggerParams['email'])
		);

		if (Debugger::isEnabled()) {
			if (isset($debuggerParams['barEnabled']) && $debuggerParams['barEnabled'] == false) {
				require_once LIBS_DIR . '/Asdf/Diagnostics/DevNullBar.php';
				Debugger::$bar = new DevNullBar();
			}
			if (isset($debuggerParams['newPath'])) {
				TracyHelpers::$newPath = $this->insertDirectoryPath($debuggerParams['newPath']);
			}
		}
	}

	private function initRobotLoader ()
	{
		$robotLoaderParams = $this->bootstrapParams['robotLoader'];
		$robotLoader = $this->createRobotLoader();
		foreach ($robotLoaderParams['directories'] as $directory) {
			$robotLoader->addDirectory($this->insertDirectoryPath($directory));
		}
		$robotLoader->autoRebuild = (int)$this->insertDirectoryPath($robotLoaderParams['autoRebuild']);
		$robotLoader->register();
	}

	private function setConfigs ()
	{
		$this->addConfig($this->configNeonPath, $this->sectionName);
		if (isset($this->bootstrapParams['config']['join'])) {
			foreach ($this->bootstrapParams['config']['join'] as $configPath) {
				$this->addConfig($this->insertDirectoryPath($configPath), $this->sectionName);
			}
		}
		if ($this->localNeonExists) {
			$this->addConfig($this->localNeonPath, $this->sectionName);
		}
	}

	/**
	 * prevede nazev adresare z %nazev% na fyzickou cestu
	 * napr. %dir%/logs/ na /var/www/project/logs/
	 *
	 * @param string $bootstrapParamValue
	 * @return string
	 */
	protected function insertDirectoryPath($bootstrapParamValue)
	{
		return preg_replace_callback(
			'~%([0-9a-zA-Z]+)%~',
			function ($matches) {
				$name = $matches[1];
				if (array_key_exists($name, $this->directories) == false) {
					throw new \RuntimeException("set directory with name '$name'");
				}
				return $this->directories[$name];
			},
			$bootstrapParamValue
		);
	}

	/**
	 * @return \SystemContainer
	 */
	public function getContainer ()
	{
		$container = parent::createContainer();
		$container->application->errorPresenter = 'Error';

		$pluginManager = $container->pluginManager;
		$container->application->onStartup[] = function (Application $sender) use ($pluginManager) {
			$pluginManager->onStartup($sender);
		};
		$container->application->onRequest[] = function (Application $sender, Request $request) use ($pluginManager) {
			$pluginManager->onRequest($sender, $request);
		};
		$container->application->onPresenter[] = function (Application $sender, Presenter $presenter) use ($pluginManager) {
			$pluginManager->onPresenterCreate($sender, $presenter);
		};
		$container->application->onResponse[] = function (Application $application, IResponse $response) use ($pluginManager) {
			$pluginManager->onResponse($application, $response);
		};
		$container->application->onShutdown[] = function (Application $application, \Exception $exception = null) use ($pluginManager) {
			$pluginManager->onShutdown($application, $exception);
		};
		return $container;
	}
}

