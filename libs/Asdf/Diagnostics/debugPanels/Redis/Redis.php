<?php
namespace Asdf\Diagnostics\Panel;
use Predis\Connection\StreamConnection;
use Tracy\IBarPanel;
use Predis\Command\CommandInterface;
use Nette\Caching\Cache;
use Nette\Object;
use Nette\Http\Response;
use Nette\Http\Request;

class Redis extends Object implements IBarPanel
{
	/**
	 * @var RedisPanelConnection
	 */
	private $connection;
	private $server;
	private $debugBuffer;

	public function __construct (
			Cache $cache,
			Request $request,
			Response $response
	)
	{
		if (isset($_POST['action'])) {
			if ($_POST['action'] == 'redisPanelFlush') {
				$cache->clean();
				$response->redirect($request->getUrl());
				exit();
			}
		}
		$this->connection = $cache->getStorage()->getClient()->getConnection();
	}


	public function getTab ()
	{
		$this->server = $this->connection->server;
		$this->debugBuffer = $this->connection->debugBuffer;
		ob_start();
		require __DIR__ . '/templates/tab.phtml';
		return ob_get_clean();
	}

	public function getId ()
	{
		return __CLASS__;
	}

	public function getPanel ()
	{
		if (0 == count($this->debugBuffer)) {
			return '';
		}
		ob_start();
		require __DIR__ . '/templates/panel.phtml';
		return ob_get_clean();
	}
}


class RedisPanelConnection extends StreamConnection
{
	private $tstart;
	public $server;
	public $debugBuffer = array();

	public function connect()
	{
		parent::connect();
		$this->server = $this->__toString();
	}

	public function writeCommand (CommandInterface $command)
	{
		$this->startTimer();
		parent::writeCommand($command);
		$command->writeDuration = $this->stopTimer();
	}

	public function readResponse (CommandInterface $command)
	{
		$this->startTimer();
		$reply = parent::readResponse($command);
		$command->readDuration = $this->stopTimer();

		$this->storeDebug($command);
		return $reply;
	}

	private function storeDebug (CommandInterface $command)
	{
		$firtsArg = $command->getArgument(0);

		if (!isset($this->debugBuffer[$firtsArg])) {
			$this->debugBuffer[$firtsArg] = array(
				'id' => $firtsArg,
				'commands' => array(),
			);
		}

		$this->debugBuffer[$firtsArg]['commands'][] = array (
			'arg' => $command->getId(),
			'write' => array('duration' => $command->writeDuration),
			'read' => array('duration' => $command->readDuration)
		);
	}

	private function startTimer()
	{
		$this->tstart = microtime(true);
	}

	private function stopTimer()
	{
		return round((microtime(true) - $this->tstart) * 1000, 4);
	}
}

