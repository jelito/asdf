<?php
namespace Asdf\Database;

use Asdf\Application\ITimeProvider;
use Nette\Object;

class TimeProvider extends Object implements ITimeProvider
{
	/**
	 * @var \Nette\DateTime
	 */
	protected $dateTime;
	
	/**
	 * @var string
	 */
	protected $isoFormated;
	
	public function __construct (\Nette\Database\Connection $db)
	{
		$data = $db->query('SELECT NOW() as dateTime')->fetch();
		$this->dateTime = $data['dateTime'];
	}
	
	/**
	 * @return \Nette\DateTime
	 */
	public function getConstantDateTime ()
	{
		return $this->dateTime;
	}
	
	/**
	 * @return int
	 */
	public function getConstantTimestamp ()
	{
		return $this->dateTime->getTimestamp();
	}
}