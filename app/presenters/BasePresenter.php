<?php

abstract class BasePresenter extends \Asdf\Application\UI\BasePresenter
{
	/**
	 * @var Asdf\Log\ILogger
	 */
	protected $logger;

	/**
	 *
	 * @var \Asdf\Localization\ITranslator
	 */
	protected $translator;

	protected function startup ()
	{
		parent::startup();
		$this->logger = $this->context->logger;
		$this->translator = $this->context->translator;
	}
}
