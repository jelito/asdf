<?php
use Nette\Diagnostics\Debugger;
use Nette\Application as NA;

class ErrorPresenter extends BasePresenter
{

	/**
	 * @param  Exception
	 * @return void
	 */
	public function renderDefault ($exception)
	{
		// AJAX request? Just note this error in payload.
		if ($this->isAjax() === TRUE) {
			$this->payload->error = TRUE;
			$this->terminate();
		} else if ($exception instanceof NA\BadRequestException) {
			$code = $exception->getCode();
			// load template 403.latte or 404.latte or ... 4xx.latte
			$this->setView((in_array($code, array(403, 404, 405, 410, 500)) === TRUE) ? $code : '4xx');
			// log to access.log

			$file = $exception->getFile();
			$line = $exception->getLine();
			$message = $exception->getMessage();
			Debugger::log("HTTP code $code: {$message} in {$file}:{$line}", 'access');
		} else {
			// load template 500.latte
			$this->setView('500');
			// and log exception
			Debugger::log($exception, Debugger::ERROR);
		}
	}

}
