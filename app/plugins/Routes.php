<?php
use Asdf\Application\Plugin\BasePlugin;
use Nette\Application\IRouter;
use Nette\Application\Routers\Route;

class RoutesPlugin extends BasePlugin
{
	public function __construct (IRouter $router)
	{
		$router[] = new Route('index.php', 'Homepage:default', $router::ONE_WAY);
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
	}
}