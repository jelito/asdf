<?php
use Asdf\Application\Plugin\BasePlugin;
use Asdf\Application\UI\BasePresenter;
use Asdf\Application\Widget\Widget;
use Asdf\Application\Widget\Placeholder;
use Nette\Bridges\ApplicationLatte\Template;

class WidgetsPlugin extends BasePlugin
{
	/**
	 * @var Placeholder
	 */
	private $placeholder;
	/**
	 * @var Widget[]
	 */
	private $widgets;

	/**
	 * @param Placeholder $placeholder
	 * @param Widget[] $widgets
	 */
	public function __construct (Placeholder $placeholder, array $widgets)
	{
		$this->placeholder = $placeholder;
		$this->widgets = $widgets;
	}

	public function onPresenterCreateTemplate (BasePresenter $presenter, Template $template)
	{
		foreach ($this->widgets as $widget) {
			$widget->setParent($presenter);
			$this->placeholder->addWidget($widget);
		}
		$template->placeholder = $this->placeholder;
	}
}