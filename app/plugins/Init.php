<?php
use Asdf\Application\Plugin\BasePlugin;

class InitPlugin extends BasePlugin
{
	public function __construct ()
	{
		setlocale(LC_TIME, "cs_CZ.utf-8");
	}
}