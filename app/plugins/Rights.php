<?php
use Nette\Application\Application;
use Nette\Http\Request;
use Asdf\Application\Plugin\BasePlugin;
use Asdf\Security\IAuthorizator;
use Asdf\Application\UI\BasePresenter;

class RightsPlugin extends BasePlugin
{
	/**
	 * @var Asdf\Security\IAuthorizator
	 */
	protected $authorizator;
	/**
	 * @var Nette\Http\Request
	 */
	protected $httpRequest;
	/**
	 * @var string
	 */
	protected $unsignedUserPage;

	/**
	 * @param IAuthorizator $authorizator
	 * @param Request $httpRequest
	 * @param string $unsignedUserPage
	 */
	public function __construct (
		IAuthorizator $authorizator,
		Request $httpRequest,
		$unsignedUserPage
	)
	{
		$this->authorizator = $authorizator;
		$this->httpRequest = $httpRequest;
		$this->unsignedUserPage = $unsignedUserPage;
	}

	/**
	 * @inheritdoc
	 */
	public function onPresenterCreate(Application $application, BasePresenter $presenter)
	{
		$resourceName = 'controller-' . strtolower($presenter->getName());
		$privilege = $presenter->getAction();

		if ($this->authorizator->hasResource($resourceName) === TRUE) {
			if (FALSE === $presenter->getUser()->isAllowed($resourceName, $privilege)) {
				if ($presenter->getUser()->isLoggedIn() === TRUE) {
					$presenter->flashMessage('Insufficient rights. Sign in another account.', 'warning');
				} else {
					$presenter->flashMessage('Insufficient rights. Sign in.', 'warning');
				}

				if ($presenter->isAjax()) {
					$presenter->forward(':sign:in');
				} else {
					$returnUrl = $this->httpRequest->getUrl()->getAbsoluteUrl();
					$presenter->redirect($this->unsignedUserPage, array('return_url' => $returnUrl));
				}
			}
		}
	}
}