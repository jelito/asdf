<?php
use Asdf\Localization\ITranslator;
use Asdf\Application\Plugin\BasePlugin;
use Asdf\Application\UI\BasePresenter;
use Asdf\Application\UI\Control;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\Http\IRequest;

class TemplatesPlugin extends BasePlugin
{
	private $translator;
	private $httpRequest;

	private $helpers;

	public function __construct (ITranslator $translator, IRequest $httpRequest)
	{
		$this->translator = $translator;
		$this->httpRequest = $httpRequest;
		$this->helpers = $this->createHelpers();
	}

	public function onControlCreateTemplate (Control $control, Template $template)
	{
		$this->initTemplate($template);
	}

	public function onPresenterCreateTemplate (BasePresenter $presenter, Template $template)
	{
		$this->initTemplate($template);
	}

	private function initTemplate(Template $template)
	{
		$template->setTranslator($this->translator);
		$template->getLatte()->addMacro('placeholder', new \Asdf\Latte\Macro\Placeholder());
		foreach ($this->helpers as $helperName => $helper) {
			$template->addFilter($helperName, $helper);
		}
		$template->urlPath = $this->httpRequest->getUrl()->getPath();

		return $template;
	}

	private function createHelpers()
	{
		$helpers = array();
		$helpers['json'] = function ($value, $options = NULL) {
			return json_encode($value, $options);
		};
		return $helpers;
	}
}