<?php
define('BASE_DIR', realpath(__DIR__ . '/../'));
require_once BASE_DIR . '/libs/Asdf/Bootstrap/Configurator.php';

$configurator = new \Asdf\Configurator;
$configurator->getContainer()->application->run();
