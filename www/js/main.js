/*****************************
 messenger
 *******************************/
function Messenger(params){ 
	var self = this;
	Object.call( self, name);
	if(params === undefined) params = {};
	
	self.place = params.place == null ? alert('Messenger - zadej parametr place'): params.place;
	self.style = params.style === undefined ? "alert" : params.style;
	self.typePrefix = params.typePrefix === undefined ? 'alert-' : params.typePrefix;
	self.autoHideDelay = params.autoHideDelay === undefined ? 0 : params.autoHideDelay;
	
	this.addMessage = function(text, params){
		if(params === undefined) params = {};
		
		var title = params.title === undefined ? null : params.title;
		var type = params.type === undefined ? null : params.type;
		var style = params.style === undefined ? self.style : params.style;
		var classes = (params.classes === undefined ? new Array() : params.classes);
		params.autoHideDelay = params.autoHideDelay === undefined ? self.autoHideDelay : params.autoHideDelay;
			
		classes.push(style);
		if(type){
			classes.push(self.typePrefix + type);
		}
		if(title === null) {
			title = '';
			classes.push("noTitle");
		}
		params.classes = classes;
		self.place.freeow(title, text, params);
	}
}