<?php
define('WWW_DIR', realpath(dirname(dirname(__FILE__))));
define('BASE_DIR', realpath(WWW_DIR . '/../'));
define('APP_DIR', realpath(BASE_DIR . '/app'));
define('LIBS_DIR', realpath(BASE_DIR . '/libs'));
define('LOG_DIR', realpath(BASE_DIR . '/log'));
define('TEMP_DIR', realpath(BASE_DIR . '/temp'));


if(isset($_POST['removeAndStart'])) {
	function rrmdir($dir) {
	    foreach(scandir($dir) as $file) {
	    	if($file == '.' || $file == '..') {
	    		continue;
	    	}
	    	$path = $dir . '/' . $file;
	        if(is_dir($path))
	            rrmdir($path);
	        else
	            unlink($path);
	    }
	    rmdir($dir);
	};
	rrmdir(WWW_DIR . '/checker');
	echo '<h1>Congratulations</h1>run<br /><code>chmod  0755 ' . WWW_DIR . '</code><br /> and <a href="../index.php">redirect</a>';
	exit();
}
/**
 * Check PHP configuration.
 */
foreach (array('function_exists', 'version_compare', 'extension_loaded', 'ini_get') as $function) {
	if (!function_exists($function)) {
		die("Error: function '$function' is required by Nette Framework and this Requirements Checker.");
	}
}



/**
 * Check assets folder, template file must be readable
 */
define('TEMPLATE_FILE', __DIR__ . '/assets/checker.phtml');
if (!is_readable(TEMPLATE_FILE)) {
	die("Error: template file is not readable. Check assets folder (part of distribution), it should be present, readable and contain readable template file.");
}



/**
 * Check Nette Framework requirements.
 */
define('CHECKER_VERSION', '1.5');

$tests = array();

$tests[] = array(
	'title' => 'inotifywait shell command',
	'required' => false,
	'passed' => command_exist('inotifywait'),
	'message' => 'Present',
	'errorMessage' => 'Absent',
	'description' => 'Inotify tools must be present for automatic testing',
);

$tests[] = array(
	'title' => 'sass shell command',
	'required' => false,
	'passed' => command_exist('sass'),
	'message' => 'Present',
	'errorMessage' => 'Absent',
	'description' => 'Sass tools must be present for automatic css translate',
);

$tests[] = addDirectoryExistsAndWritableCheck('checker', WWW_DIR);
$tests[] = addDirectoryExistsAndWritableCheck('checker', WWW_DIR . '/checker');
$tests[] = addDirectoryExistsAndWritableCheck('log', LOG_DIR);
$tests[] = addDirectoryExistsAndWritableCheck('log debug', LOG_DIR . '/debug');
$tests[] = addDirectoryExistsAndWritableCheck('temp', TEMP_DIR);
$tests[] = addDirectoryExistsAndWritableCheck('temp cache', TEMP_DIR . '/cache');
$tests[] = addFileExistsCheck('local.neon', APP_DIR . '/config/local.neon', BASE_DIR . '/docs/local.neon');

$tests[] = array(
	'title' => 'Web server',
	'message' => $_SERVER['SERVER_SOFTWARE'],
);

$tests[] = array(
	'title' => 'PHP version',
	'required' => TRUE,
	'passed' => version_compare(PHP_VERSION, '5.3.0', '>='),
	'message' => PHP_VERSION,
	'description' => 'Your PHP version is too old. Nette Framework requires at least PHP 5.3.0 or higher.',
);

$tests[] = array(
	'title' => 'Memory limit',
	'message' => ini_get('memory_limit'),
);

$tests['hf'] = array(
	'title' => '.htaccess file protection',
	'required' => FALSE,
	'description' => 'File protection by <code>.htaccess</code> is not present. You must be careful to put files into document_root folder.',
	'script' => '<script src="assets/denied/checker.js"></script> <script>displayResult("hf", typeof fileProtectionChecker == "undefined")</script>',
);

$tests['hr'] = array(
	'title' => '.htaccess mod_rewrite',
	'required' => FALSE,
	'description' => 'Mod_rewrite is probably not present. You will not be able to use Cool URL.',
	'script' => '<script src="assets/rewrite/checker"></script> <script>displayResult("hr", typeof modRewriteChecker == "boolean")</script>',
);

$tests[] = array(
	'title' => 'Function ini_set()',
	'required' => FALSE,
	'passed' => function_exists('ini_set'),
	'description' => 'Function <code>ini_set()</code> is disabled. Some parts of Nette Framework may not work properly.',
);

$tests[] = array(
	'title' => 'Function error_reporting()',
	'required' => TRUE,
	'passed' => function_exists('error_reporting'),
	'description' => 'Function <code>error_reporting()</code> is disabled. Nette Framework requires this to be enabled.',
);

$tests[] = array(
	'title' => 'Function flock()',
	'required' => TRUE,
	'passed' => flock(fopen(__FILE__, 'r'), LOCK_SH),
	'description' => 'Function <code>flock()</code> is not supported on this filesystem. Nette Framework requires this to process atomic file operations.',
);

$tests[] = array(
	'title' => 'Register_globals',
	'required' => TRUE,
	'passed' => !iniFlag('register_globals'),
	'message' => 'Disabled',
	'errorMessage' => 'Enabled',
	'description' => 'Configuration directive <code>register_globals</code> is enabled. Nette Framework requires this to be disabled.',
);

$tests[] = array(
	'title' => 'Zend.ze1_compatibility_mode',
	'required' => TRUE,
	'passed' => !iniFlag('zend.ze1_compatibility_mode'),
	'message' => 'Disabled',
	'errorMessage' => 'Enabled',
	'description' => 'Configuration directive <code>zend.ze1_compatibility_mode</code> is enabled. Nette Framework requires this to be disabled.',
);

$tests[] = array(
	'title' => 'Variables_order',
	'required' => TRUE,
	'passed' => strpos(ini_get('variables_order'), 'G') !== FALSE && strpos(ini_get('variables_order'), 'P') !== FALSE && strpos(ini_get('variables_order'), 'C') !== FALSE,
	'description' => 'Configuration directive <code>variables_order</code> is missing. Nette Framework requires this to be set.',
);

$tests[] = array(
	'title' => 'Session auto-start',
	'required' => FALSE,
	'passed' => session_id() === '' && !defined('SID'),
	'description' => 'Session auto-start is enabled. Nette Framework recommends not to use this directive for security reasons.',
);

$tests[] = array(
	'title' => 'Reflection extension',
	'required' => TRUE,
	'passed' => class_exists('ReflectionFunction'),
	'description' => 'Reflection extension is required.',
);

$tests[] = array(
	'title' => 'SPL extension',
	'required' => TRUE,
	'passed' => extension_loaded('SPL'),
	'description' => 'SPL extension is required.',
);

$tests[] = array(
	'title' => 'PCRE extension',
	'required' => TRUE,
	'passed' => extension_loaded('pcre') && @preg_match('/pcre/u', 'pcre'),
	'message' => 'Enabled and works properly',
	'errorMessage' => 'Disabled or without UTF-8 support',
	'description' => 'PCRE extension is required and must support UTF-8.',
);

$tests[] = array(
	'title' => 'ICONV extension',
	'required' => TRUE,
	'passed' => extension_loaded('iconv') && (ICONV_IMPL !== 'unknown') && @iconv('UTF-16', 'UTF-8//IGNORE', iconv('UTF-8', 'UTF-16//IGNORE', 'test')) === 'test',
	'message' => 'Enabled and works properly',
	'errorMessage' => 'Disabled or does not work properly',
	'description' => 'ICONV extension is required and must work properly.',
);

$tests[] = array(
	'title' => 'PHP tokenizer',
	'required' => TRUE,
	'passed' => extension_loaded('tokenizer'),
	'description' => 'PHP tokenizer is required.',
);

$tests[] = array(
	'title' => 'PDO extension',
	'required' => FALSE,
	'passed' => $pdo = extension_loaded('pdo') && PDO::getAvailableDrivers(),
	'message' => $pdo ? 'Available drivers: ' . implode(' ', PDO::getAvailableDrivers()) : NULL,
	'description' => 'PDO extension or PDO drivers are absent. You will not be able to use <code>Nette\Database</code>.',
);

$tests[] = array(
	'title' => 'Multibyte String extension',
	'required' => FALSE,
	'passed' => extension_loaded('mbstring'),
	'description' => 'Multibyte String extension is absent. Some internationalization components may not work properly.',
);

$tests[] = array(
	'title' => 'Multibyte String function overloading',
	'required' => TRUE,
	'passed' => !extension_loaded('mbstring') || !(mb_get_info('func_overload') & 2),
	'message' => 'Disabled',
	'errorMessage' => 'Enabled',
	'description' => 'Multibyte String function overloading is enabled. Nette Framework requires this to be disabled. If it is enabled, some string function may not work properly.',
);

$tests[] = array(
	'title' => 'Memcache extension',
	'required' => FALSE,
	'passed' => extension_loaded('memcache'),
	'description' => 'Memcache extension is absent. You will not be able to use <code>Nette\Caching\Storages\MemcachedStorage</code>.',
);

$tests[] = array(
	'title' => 'GD extension',
	'required' => FALSE,
	'passed' => extension_loaded('gd'),
	'description' => 'GD extension is absent. You will not be able to use <code>Nette\Image</code>.',
);

$tests[] = array(
	'title' => 'Bundled GD extension',
	'required' => FALSE,
	'passed' => extension_loaded('gd') && GD_BUNDLED,
	'description' => 'Bundled GD extension is absent. You will not be able to use some functions such as <code>Nette\Image::filter()</code> or <code>Nette\Image::rotate()</code>.',
);

$tests[] = array(
	'title' => 'Fileinfo extension or mime_content_type()',
	'required' => FALSE,
	'passed' => extension_loaded('fileinfo') || function_exists('mime_content_type'),
	'description' => 'Fileinfo extension or function <code>mime_content_type()</code> are absent. You will not be able to determine mime type of uploaded files.',
);

$tests[] = array(
	'title' => 'HTTP_HOST or SERVER_NAME',
	'required' => TRUE,
	'passed' => isset($_SERVER["HTTP_HOST"]) || isset($_SERVER["SERVER_NAME"]),
	'message' => 'Present',
	'errorMessage' => 'Absent',
	'description' => 'Either <code>$_SERVER["HTTP_HOST"]</code> or <code>$_SERVER["SERVER_NAME"]</code> must be available for resolving host name.',
);

$tests[] = array(
	'title' => 'REQUEST_URI or ORIG_PATH_INFO',
	'required' => TRUE,
	'passed' => isset($_SERVER["REQUEST_URI"]) || isset($_SERVER["ORIG_PATH_INFO"]),
	'message' => 'Present',
	'errorMessage' => 'Absent',
	'description' => 'Either <code>$_SERVER["REQUEST_URI"]</code> or <code>$_SERVER["ORIG_PATH_INFO"]</code> must be available for resolving request URL.',
);

$tests[] = array(
	'title' => 'DOCUMENT_ROOT & SCRIPT_FILENAME or SCRIPT_NAME',
	'required' => TRUE,
	'passed' => isset($_SERVER['DOCUMENT_ROOT'], $_SERVER['SCRIPT_FILENAME']) || isset($_SERVER['SCRIPT_NAME']),
	'message' => 'Present',
	'errorMessage' => 'Absent',
	'description' => '<code>$_SERVER["DOCUMENT_ROOT"]</code> and <code>$_SERVER["SCRIPT_FILENAME"]</code> or <code>$_SERVER["SCRIPT_NAME"]</code> must be available for resolving script file path.',
);

$tests[] = array(
	'title' => 'SERVER_ADDR or LOCAL_ADDR',
	'required' => TRUE,
	'passed' => isset($_SERVER["SERVER_ADDR"]) || isset($_SERVER["LOCAL_ADDR"]),
	'message' => 'Present',
	'errorMessage' => 'Absent',
	'description' => '<code>$_SERVER["SERVER_ADDR"]</code> or <code>$_SERVER["LOCAL_ADDR"]</code> must be available for detecting development / production mode.',
);

paint($tests);



/**
 * Paints checker.
 * @param  array
 * @return void
 */
function paint($requirements)
{
	$errors = $warnings = FALSE;

	foreach ($requirements as $id => $requirement)
	{
		$requirements[$id] = $requirement = (object) $requirement;
		if (isset($requirement->passed) && !$requirement->passed) {
			if ($requirement->required) {
				$errors = TRUE;
			} else {
				$warnings = TRUE;
			}
		}
	}

	require TEMPLATE_FILE;
}


function addFileExistsCheck($fileName, $filePath, $sourceFilePath = null)
{
	if($sourceFilePath) {
		$description = "<code>cp $sourceFilePath $filePath</code>";
	} else {
		$description = 'Create file ' . $filePath;
	}

	return array(
		'title' => $fileName . ' exists',
		'required' => TRUE,
		'passed' => file_exists($filePath),
		'message' => 'Present',
		'errorMessage' => 'File doesn\'t exists',
		'description' => $description,
	);
}

function addDirectoryExistsAndWritableCheck($dirName, $dirPath)
{
	$fileExists = $fileWritable = true;
	$description = '';
	if (file_exists($dirPath)) {
		if (!is_writable($dirPath)) {
			$fileWritable = false;
			$description = "<code>chmod o+w $dirPath -R</code>";
		}
	} else {
		$fileExists = false;
		$description = "<code>mkdir $dirPath </code><br /><code>chmod o+w $dirPath -R</code>";
	}

	return array(
		'title' => $dirName . ' dir exists and writable',
		'required' => TRUE,
		'passed' => $fileWritable,
		'message' => 'Present and writable',
		'errorMessage' => 'Doesn\'t exists or not writable',
		'description' => $description
	);
}

function command_exist($cmd) {
	$returnVal = shell_exec("which $cmd");
	return (empty($returnVal) ? false : true);
}


/**
 * Gets the boolean value of a configuration option.
 * @param  string  configuration option name
 * @return bool
 */
function iniFlag($var)
{
	$status = strtolower(ini_get($var));
	return $status === 'on' || $status === 'true' || $status === 'yes' || (int) $status;
}
