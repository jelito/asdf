final class Helpers
{

	###############################################
	/**
	 * pokud je projekt prochazen napr. pres sambu
	 * tak realna cesta bude linuxovska /var/www/.....
	 * ale cesta ve windows bude z:/www/....
	 * v teto promene bude windows cesta k projektu a v ladence se misto linux cesty objevi ona
	 *
	 * @var string
	 */
	public static $newPath = null;
	###############################################

	public static function editorLink($file, $line, $text = null)
	{
		if (Debugger::$editor && is_file($file)) {
			$dir = dirname(strtr($file, '/', DIRECTORY_SEPARATOR));
			$base = isset($_SERVER['SCRIPT_FILENAME']) ? dirname(dirname(strtr($_SERVER['SCRIPT_FILENAME'], '/', DIRECTORY_SEPARATOR))) : dirname($dir);

			###############################################
			if(self::$newPath){
				$file = str_replace($base, self::$newPath, $file);
			}
			###############################################

			if (substr($dir, 0, strlen($base)) === $base) {
				$dir = '...' . substr($dir, strlen($base));
			}

			..........
	}


